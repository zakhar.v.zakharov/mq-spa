﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Jobs;
using MetaQuotes.Playground.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaQuotes.Playground.Benchmarks
{    
    [MinColumn, MaxColumn]    
    public class DatabaseBenchmark
    {        
        IDataContext _dataContext;
        public DatabaseBenchmark()
        {
            var dataContext = new DataContext(new Database.Models.DatabaseOptions("geobase.dat", false), new LoggingAdapter<DataContext>(true));
            _dataContext = dataContext;
        }
        [Benchmark]
        public void LoadDatabase()
        {
            _dataContext.Load();
        }
    }
}
