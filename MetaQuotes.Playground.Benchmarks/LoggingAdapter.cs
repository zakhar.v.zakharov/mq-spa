﻿using MetaQuotes.Playground.Database;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Benchmarks
{
    public class LoggingAdapter<T> : ILogger<T>
    {
        private readonly BenchmarkDotNet.Loggers.ILogger _logger;
        private readonly bool _isSilent;

        /// <summary>
        /// Initializes logging adapter to use Microsoft.Extensions.Logging.ILogger in Benchmarks
        /// </summary>
        /// <param name="isSilent">Pass true if you don't want logs to appear in console</param>
        public LoggingAdapter(bool isSilent = false)
        {            
            _logger = new BenchmarkDotNet.Loggers.ConsoleLogger();
            _isSilent = isSilent;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }
        Dictionary<LogLevel, BenchmarkDotNet.Loggers.LogKind> levelsMapping = new Dictionary<LogLevel, BenchmarkDotNet.Loggers.LogKind>
        {
            [LogLevel.Information] =BenchmarkDotNet.Loggers.LogKind.Info,
            [LogLevel.Error] = BenchmarkDotNet.Loggers.LogKind.Error,
            [LogLevel.Trace] = BenchmarkDotNet.Loggers.LogKind.Statistic,
            [LogLevel.Debug] = BenchmarkDotNet.Loggers.LogKind.Default,
            [LogLevel.Warning] = BenchmarkDotNet.Loggers.LogKind.Error            
        };

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!_isSilent)
            {
                var kind = levelsMapping[logLevel];
                _logger.WriteLine(kind, formatter(state, exception));
            }
        }
    }
}
