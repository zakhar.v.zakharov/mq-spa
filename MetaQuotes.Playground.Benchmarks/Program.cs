﻿using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;
using System;
using System.Linq;

namespace MetaQuotes.Playground.Benchmarks
{
    class Program
    {

        static IConfig GetConfig(int launchCount, int iterations)
        {
            var config = DefaultConfig.Instance
                .KeepBenchmarkFiles()
                .WithOptions(ConfigOptions.DisableOptimizationsValidator)
                .AddExporter(CsvMeasurementsExporter.Default, RPlotExporter.Default)
                .AddColumn(StatisticColumn.Max, StatisticColumn.Min, StatisticColumn.Mean, StatisticColumn.OperationsPerSecond)                
                .AddJob(Job.Default
                    .WithRuntime(CoreRuntime.Core31)
                    .WithLaunchCount(launchCount)
                    .WithPlatform(Platform.AnyCpu)
                    .WithMaxIterationCount(iterations)
                    );
            return config;
        }
        static void Main(string[] args)
        {                                                   
          BenchmarkRunner.Run<DatabaseBenchmark>(GetConfig(10,100));                        
          BenchmarkRunner.Run<SearchByStringBenchmark>(GetConfig(2,20));
          BenchmarkRunner.Run<SearchByIpBenchmark>(GetConfig(2, 20));
        }
    }
}
