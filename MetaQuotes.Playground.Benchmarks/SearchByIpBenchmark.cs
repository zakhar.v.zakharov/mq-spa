﻿using BenchmarkDotNet.Attributes;
using MetaQuotes.Playground.Database;
using MetaQuotes.Playground.Services;
using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Services.Search;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaQuotes.Playground.Benchmarks
{
    [MinColumn, MaxColumn]
    public class SearchByIpBenchmark
    {
        [Params(20000, 40000, 60000, 80000, 100000)]
        public int N;
        ILocationService _locationService;
        IDataContext _dataContext;
        int records = 0;

        int[] randomNumbers;
        public SearchByIpBenchmark()
        {
            var dataContext = new DataContext(new Database.Models.DatabaseOptions("geobase.dat", true), new LoggingAdapter<DataContext>(true));
            _dataContext = dataContext;
            var factoryMock = new Mock<ISearchFactory>();            
            factoryMock.Setup(c => c.GetSearchService<(uint, uint)>()).Returns(new SearchByRangeService());
            _locationService = new LocationService(dataContext, factoryMock.Object, new LoggingAdapter<LocationService>(true));
            records = dataContext.Header.Records;
            GenerateRandomNumbers(2);
        }

        /// <summary>
        /// Being called for each N
        /// </summary>
        [GlobalSetup]
        public void GlobalSetup()
        {
            var dataContextMock = new Mock<IDataContext>();
            var ranges = _dataContext.Ranges.Take(N).ToArray();            
            var factoryMock = new Mock<ISearchFactory>();
            factoryMock.Setup(c => c.GetSearchService<(uint, uint)>()).Returns(new SearchByRangeService());
            dataContextMock.Setup(c => c.Locations).Returns(_dataContext.Locations);
            dataContextMock.Setup(c => c.Ranges).Returns(ranges);
            records = N;
            _locationService = new LocationService(dataContextMock.Object, factoryMock.Object, new LoggingAdapter<LocationService>(true));
            GenerateRandomNumbers(2);
        }

        /// <summary>
        /// Generates random sequence
        /// </summary>
        /// <param name="count">Size of random numbers array</param>
        private void GenerateRandomNumbers(int count)
        {
            randomNumbers = new int[count];
            var rnd = new Random();
            for (var i = 0; i < count; i++)
            {
                randomNumbers[i] = rnd.Next(0, records);
            }
        }

        public IEnumerable<uint> IpAddress()
        {
            var rnd = new Random();
            return randomNumbers.Select(r => (uint)rnd.Next((int)_dataContext.Ranges[r].IpFrom, (int)_dataContext.Ranges[r].IpTo));
        }

        [Benchmark]
        [ArgumentsSource(nameof(IpAddress))]
        public void SearchByIp(uint ip)
        {
            var result = _locationService.GetLocationsByIpRange(ip);
        }
    }
}
