﻿using BenchmarkDotNet.Attributes;
using MetaQuotes.Playground.Database;
using MetaQuotes.Playground.Services;
using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Services.Search;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaQuotes.Playground.Benchmarks
{
    [MinColumn, MaxColumn]
    public class SearchByStringBenchmark
    {
        [Params(20000, 40000, 60000, 80000,100000)]
        public int N;
        ILocationService _locationService;
        IDataContext _dataContext;
        int records = 10000;

        int[] randomNumbers;
        public SearchByStringBenchmark()
        {
            var dataContext = new DataContext(new Database.Models.DatabaseOptions("geobase.dat", true), new LoggingAdapter<DataContext>(true));
            _dataContext = dataContext;
            var factoryMock = new Mock<ISearchFactory>();
            factoryMock.Setup(c => c.GetSearchService<string>()).Returns(new SearchByStringService());            
            _locationService = new LocationService(dataContext, factoryMock.Object, new LoggingAdapter<LocationService>(true));
            records = dataContext.Header.Records;            
            GenerateRandomNumbers(2);
        }

        /// <summary>
        /// Being called for each N
        /// </summary>
        [GlobalSetup]
        public void GlobalSetup()
        {
            var dataContextMock = new Mock<IDataContext>();
            var locations = _dataContext.Locations.Take(N).ToArray();
            var indexes = _dataContext.ByCityLocationIndex.Where(i => i < N).ToArray();
            var factoryMock = new Mock<ISearchFactory>();
            factoryMock.Setup(c => c.GetSearchService<string>()).Returns(new SearchByStringService());
            dataContextMock.Setup(c => c.Locations).Returns(locations);
            dataContextMock.Setup(c => c.ByCityLocationIndex).Returns(indexes);
            records = N;
            _locationService = new LocationService(dataContextMock.Object, factoryMock.Object, new LoggingAdapter<LocationService>(true));
            GenerateRandomNumbers(2);
        }

        /// <summary>
        /// Generates random sequence
        /// </summary>
        /// <param name="count">Size of random numbers array</param>
        private void GenerateRandomNumbers(int count)
        {
            randomNumbers = new int[count];
            var rnd = new Random();
            for (var i = 0; i < count; i++)
            {
                randomNumbers[i] = rnd.Next(0, records);
            }
        }

        public IEnumerable<string> Cities()
        {
            return randomNumbers.Select(r => _dataContext.Locations[r].City);            
        }

        [Benchmark]
        [ArgumentsSource(nameof(Cities))]
        public void SearchByCity(string city)
        {
            var result = _locationService.GetLocationsByCityName(city);
        }
        
    }
}
