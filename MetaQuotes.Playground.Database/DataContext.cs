﻿using MetaQuotes.Playground.Database.Extensions;
using MetaQuotes.Playground.Database.Models;
using MetaQuotes.Playground.Database.Models.Unsafe;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace MetaQuotes.Playground.Database
{
    public class DataContext : IDataContext
    {
        public DataContext(DatabaseOptions options, ILogger<DataContext> logger)
        {
            _fileName = options.FileName;
            _logger = logger;
            if(options.LoadImmediately)
                Load();
        }        
        private readonly string _fileName;
        private readonly ILogger<DataContext> _logger;
        private Header _header;
        private IpRange[] _ranges;
        private Location[] _locations;
        private uint[] _indexes;
        byte[] _buffer;

        public Header Header { get => _header; }
        public IpRange[] Ranges { get => _ranges; }
        public Location[] Locations { get => _locations; }
        public uint[] ByCityLocationIndex { get => _indexes; }

        /// <summary>
        /// Loads data from file to memory
        /// </summary>
        public void Load()
        {            
            _logger.LogInformation("Starting database initialization");
            _logger.LogTrace("Starting loading file {FileName}", _fileName);
            _buffer = File.ReadAllBytes(_fileName);
            _logger.LogTrace("File {FileName} has been loaded", _fileName);
            var span = _buffer.AsSpan();           
            LoadHeader(span);                 
            LoadIpRanges(span);            
            LoadLocations(span);            
            LoadIndexes(span);
            _logger.LogInformation("Data has been loaded");            
        }

        private void LoadHeader(Span<byte> span)
        {
            _logger.LogTrace("Starting header deserialization");
            span.Slice(0, 60).GetObjectUnsafe<Header>(out _header);
            _logger.LogTrace("Header has been deserialized");
        }
       
        private void LoadIpRanges(Span<byte> span)
        {
            _logger.LogTrace("Starting ip ranges deserialization");
            var seq = span.Slice((int)_header.OffsetRanges, _header.Records * Marshal.SizeOf<IpRange>());
            seq.GetArrayUnsafe(_header.Records, out _ranges);
            _logger.LogTrace("Ip Ranges have been deserialized");
        }      

        private void LoadLocations(Span<byte> span)
        {
            _logger.LogTrace("Starting locations deserialization");
            var seq = span.Slice((int)_header.OffsetLocations, _header.Records * Marshal.SizeOf<Location>());            
            seq.GetArrayUnsafe(_header.Records, out _locations);
            _logger.LogTrace("Locations have been deserialized");
        }

        private void LoadIndexes(Span<byte> span)
        {
            _logger.LogTrace("Starting indexes deserialization");
            var seq = span.Slice((int)_header.OffsetCities, _header.Records * Marshal.SizeOf<uint>());            
            seq.GetArrayUnsafe(_header.Records, out _indexes);
            _logger.LogTrace("Indexes have been deserialized");
            //normalize indices
            var size = Marshal.SizeOf<Location>();
            for(int i = 0; i < _indexes.Length; i++)
            {
                _indexes[i] = (uint)(_indexes[i] / size);
            }
        }        
    }
}
