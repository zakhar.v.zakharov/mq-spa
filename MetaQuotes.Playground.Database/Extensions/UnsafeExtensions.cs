﻿using MetaQuotes.Playground.Database.Models.Unsafe;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace MetaQuotes.Playground.Database.Extensions
{
    internal static class UnsafeExtensions
    {
       
        /// <summary>
        /// Generates array from bytes sequence
        /// </summary>
        /// <typeparam name="T">Unmanaged type of array element</typeparam>
        /// <param name="bytes">Bytes sequence</param>
        /// <param name="length">Length of result array</param>
        /// <param name="mangagedArray">result</param>        
        internal static unsafe void GetArrayUnsafe<T>(this Span<byte> bytes, int length, out T[] mangagedArray) where T: unmanaged
        {            
            mangagedArray = new T[length];
            
            fixed (byte* pBuffer = bytes)
            {
                for (int i = 0; i < mangagedArray.Length; i++)
                {
                    var index = i;                    
                    mangagedArray[i] = ((T*)pBuffer)[index];
                }
            }
        }

        /// <summary>
        /// Generates object from bytes sequence
        /// </summary>
        /// <typeparam name="T">Unmanaged type of object</typeparam>
        /// <param name="bytes">Bytes sequence</param>
        /// <param name="obj">result</param>
        internal static unsafe void GetObjectUnsafe<T>(this Span<byte> bytes, out T obj) where T : unmanaged
        {         
            fixed (byte* pBuffer = bytes)
            {               
                    obj = ((T*)pBuffer)[0];
            }            
        }
    }
}
