﻿using MetaQuotes.Playground.Database.Models.Unsafe;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Database
{
    public interface IDataContext
    {
        Header Header { get; }
        IpRange[] Ranges { get; }
        Location[] Locations { get; }
        uint[] ByCityLocationIndex { get; }

        /// <summary>
        /// Loads data from file to memory
        /// </summary>
        void Load();
    }
}
