﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Database.Models
{
    public class DatabaseOptions
    {
        public DatabaseOptions() { }
        public DatabaseOptions(string fileName, bool loadImmediately = false)
        {
            FileName = fileName;
            LoadImmediately = loadImmediately;
        }
        public string FileName { get; set; }

        public bool LoadImmediately { get; set; }
    }
}
