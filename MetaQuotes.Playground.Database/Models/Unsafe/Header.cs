﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace MetaQuotes.Playground.Database.Models.Unsafe
{

    [StructLayout(LayoutKind.Explicit, Pack = 32, CharSet = CharSet.Unicode)]
    public unsafe struct Header
    {
        [FieldOffset(0)]
        private int version;
        [FieldOffset(0x4)]
        private fixed sbyte name[32];
        [FieldOffset(36)]
        private ulong timestamp;
        [FieldOffset(44)]
        private int records;
        [FieldOffset(48)]
        private uint offset_ranges;
        [FieldOffset(52)]
        private uint offset_cities;
        [FieldOffset(56)]
        private uint offset_locations;
        public string Name
        {
            get
            {
                fixed (sbyte* namePtr = name)
                {
                    return new string(namePtr);
                }
            }
        }
        public int Version { get => version; } 
        public int Records { get => records; } 
        public uint OffsetRanges { get => offset_ranges; } 
        public uint OffsetCities { get => offset_cities; } 
        public uint OffsetLocations { get => offset_locations; } 
                
        public ulong Timestamp { get => timestamp; }

    }
}
