﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace MetaQuotes.Playground.Database.Models.Unsafe
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public unsafe struct IpRange
    {
        private uint ip_from { get; set; }
        private uint ip_to { get; set; }
        private uint location_index { get; set; }

        public uint IpFrom { get => ip_from; }
        public uint IpTo { get => ip_to; }
        public uint LocationIndex { get => location_index; }
    }
}
