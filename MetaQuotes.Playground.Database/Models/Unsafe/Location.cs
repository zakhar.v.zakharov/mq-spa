﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace MetaQuotes.Playground.Database.Models.Unsafe
{   
    [StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode, Pack = 32)]
    public unsafe struct Location
    {
        [FieldOffset(0)]
        private fixed sbyte country[8];
        [FieldOffset(8)]
        private fixed sbyte region[12];
        [FieldOffset(20)]
        private fixed sbyte postal[12];
        [FieldOffset(32)]
        private fixed sbyte city[24];
        [FieldOffset(56)]
        private fixed sbyte organization[32];
        [FieldOffset(88)]
        private float latitude;
        [FieldOffset(92)]
        private float longitude;

        public string Country
        {
            get
            {
                fixed (sbyte* ptr = country)
                {
                    return new string(ptr);
                }
            }
        }

        public string Region
        {
            get
            {
                fixed (sbyte* ptr = region)
                {
                    return new string(ptr);
                }
            }
        }

        public string Postal
        {
            get
            {
                fixed (sbyte* ptr = postal)
                {
                    return new string(ptr);
                }
            }
        }
        public string City
        {
            get
            {
                fixed (sbyte* ptr = city)
                {
                    return new string(ptr);
                }
            }
        }

        public string Organization
        {
            get
            {
                fixed (sbyte* ptr = organization)
                {
                    return new string(ptr);
                }
            }
        }

        public float Latitude
        {
            get => latitude;
        }

        public float Longitude
        {
            get => longitude;
        }

    }
}
