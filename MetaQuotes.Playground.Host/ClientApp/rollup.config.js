import {terser} from 'rollup-plugin-terser';
import htmlTemplate from 'rollup-plugin-generate-html-template';
import typescript  from '@rollup/plugin-typescript';
import scss from 'rollup-plugin-scss';

const pkg = require('./package.json');
const isProduction = process.env.NODE_ENV === 'production';
const plugins = [];
const scssOptions = {output:'dist/index.css'};

if(isProduction) {
  plugins.push(terser());
  scssOptions.outputStyle = 'compressed';
}

export default [{
    input: pkg.main,    
    plugins: [typescript(), 
              scss(scssOptions), 
              htmlTemplate({template:"index.html"})],    
    output:{
        name: 'prod',        
        sourcemap:!isProduction?true:false,        
        dir: pkg.output,
        format: 'iife',        
        plugins: plugins,                
        entryFileNames : isProduction?'index-[hash].min.js':'index.js'
        }        
    }]
   