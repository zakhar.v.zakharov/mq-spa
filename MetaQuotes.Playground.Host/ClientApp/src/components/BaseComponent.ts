export default abstract class BaseComponent {
    constructor(protected container: Element) {
        this.container = container;
    }

    setTitle(title) {
        document.title = title;
    }

    public element: Element;

    protected async render() {
        let markup = this.markup();
        let markupContainer = document.createElement('div');
        markupContainer.innerHTML = markup;
        this.container.innerHTML = '';
        this.element = markupContainer;
        this.container.appendChild(markupContainer);
    }

    abstract markup();
}