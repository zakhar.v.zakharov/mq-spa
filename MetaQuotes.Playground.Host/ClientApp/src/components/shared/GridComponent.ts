import BaseComponent from "../BaseComponent";

export interface GridComponentOptions {
    columns: GridColumn[];
}
export interface GridColumn {
    header?: string;
    key: string;
}
/**
   * Universal component for Grid.
   * options: { columns:[{header:string, key:string}] }
   * user render(data:[]) method to refresh data
   */
export default class extends BaseComponent {
    constructor(protected containter: Element,
        protected options: GridComponentOptions) {
        super(containter);
    }

    data: any[];
    buildHeaders() {
        let headers = this.options.columns.map(element =>
            `<th class="header">${element.header ?? element.key}</th>`)
            .join(' ');

        return headers;
    }

    buildRows() {
        let trs = '';
        this.data.forEach(element => {
            let tds = this.options.columns.map(col => `<td>${element[col.key]}</td>`).join(' ');
            let tr = `<tr>${tds}</tr>`;
            trs += tr;
        });
        return trs;
    }

    markup() {
        if (!this.data)
            return '';
        else if (this.data.length == 0)
            return '<p>No records</p>';

        let ths = this.buildHeaders();
        let trs = this.buildRows();

        var markup = `
        <table>
            <thead>
                <tr>
                    ${ths}
                </tr>
            </thead>
            <tbody>            
                ${trs}
            </tbody>
        </table>`;
        return markup;
    }

    public async renderData(data: any[]) {
        this.data = data;
        super.render();
    }

}