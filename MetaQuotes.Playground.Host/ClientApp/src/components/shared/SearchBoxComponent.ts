import BaseComponent from "../BaseComponent";

export interface SearchBoxComponentOptions {
    pattern?: string;
    placeholder?: string;
    initValue?: string;
}
/**
   * Universal component for Search box.
   * options: { initValue:string, pattern:string, placeholder:string }
   * Subscribe to text change using subscribe(callback) method
   */
export default class extends BaseComponent {
    constructor(protected containter: Element,
        protected options: SearchBoxComponentOptions) {
        super(containter);
    }


    async render() {
        super.render();
        this.container.addEventListener('submit', (evt) => {
            evt.preventDefault();
            var val = this.container.querySelector('.search-text')["value"];
            this.callbacks.forEach(callback => {
                callback(val);
            });
        })
    }

    callbacks = [];

    subscribe(callback) {
        this.callbacks.push(callback);
    }

    markup() {
        let pattern = '';
        if (this.options.pattern) {
            pattern = `pattern="${this.options.pattern}"`;
        }
        return `        
        <form class="search-form">
            <input type="text" class="search-text" ${pattern} placeholder="${this.options.placeholder ? this.options.placeholder : ''}" value="${this.options.initValue ? this.options.initValue : ''}" />
            <input type="submit" class="search-button" value="Search" />
        </form>
        `;
    }
}