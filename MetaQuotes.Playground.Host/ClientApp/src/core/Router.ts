import BaseView from "../views/BaseView";

const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/")
    .replace(/:\?\w+/g, "?(.+)?")
    .replace(/:\w+/g, "(.+)") + "$");


export interface Route {
    path: string;
    view?: new (...pars) => BaseView;
    fallbackIfNoMatches?:boolean;
}

export default class {

    constructor(protected routes: Route[], protected appContainer:Element) {                
    }
        

    private potentialMatches(url:string) {
        return this.routes.map(route => {
            return {
                route: route,
                result: url.match(pathToRegex(route.path))
            };
        });
    }

    public getParams(match) {
        const values = match.result.slice(1);
        const keys = Array.from(match.route.path.matchAll(/:\??(\w+)/g)).map(result => result[1]);

        return Object.fromEntries(keys.map((key, i) => {
            return [key, values[i]];
        }));
    }

    currentRoute = "/";

    public setRoute(route, params) {
        var url = route;
        if (params) {
            for (var l in params) {
                url = url.replace(`:?${l}`, params[l]).replace(`:${l}`, params[l]);
            }
        }
        history.pushState(null, null, url);
    }

    public navigateTo(url) {
        history.pushState(null, null, url);
        this.route();
    }

    public getMatch(url:string){
        let match = this.potentialMatches(url).find(potentialMatch => potentialMatch.result !== null);

        if (!match) {
            // If no matches, find fallback route
            let routeToFallback = this.routes.find(r => r.fallbackIfNoMatches);
            if(routeToFallback)
            match = {
                route: routeToFallback,
                result: [routeToFallback.path]
            };
            else {
                match = {
                    route: this.routes[0],
                    result: [location.pathname]
                };
            }
        }
        return match;
    }

    public async route() {
        let match = this.getMatch(location.pathname);
        this.currentRoute = match.route.path;        
        
        const view = new match.route.view(this.appContainer, this, this.getParams(match));
        view.spinnerState(true);
        await view.render();
        view.spinnerState(false);
    }


}