import { MetaHeader } from '../models/MetaHeader';
import Urls from './Urls';
export default class {
    constructor(params) {
        this.params = params;
    }
    params:any;
    static async getLocationsByCityName(cityName): Promise<any[]>{        
        let resp = await fetch(Urls.locationsByCityUrl(cityName));
        let data = await resp.json();
        return data;
    }

    static async getLocationsByIp(ip): Promise<any[]>{
        let resp = await fetch(Urls.locationsByIpUrl(ip));
        let data = await resp.json();
        return data;
    }

    static async getDbMetaInfo(): Promise<MetaHeader> {
        let resp = await fetch(Urls.metaInfoUrl);
        let data = await resp.json();
        return data;
    }
}