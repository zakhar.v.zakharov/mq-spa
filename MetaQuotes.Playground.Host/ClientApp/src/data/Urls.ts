export default class {
static locationsByCityUrl(cityName){    
    return `/city/locations?city=${cityName}`;
}
static locationsByIpUrl(ip){    
    return `/ip/location?ip=${ip}`;
}
static get metaInfoUrl(){
    return "/meta/db";
}

}