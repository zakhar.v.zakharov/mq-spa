import './styles/index.scss';
import Router, { Route } from "./core/Router";
import HomePageView from "./views/HomePageView";
import SearchByCityView from "./views/SearchByCityView";
import SearchByIpView from "./views/SearchByIpView";


const routes:Route[] = [
    { path: "/", view: HomePageView, fallbackIfNoMatches:true },
    { path: "/locations/bycity/:?city", view: SearchByCityView },
    { path: "/locations/byip/:?ip", view: SearchByIpView }        
];

const router = new Router(routes, document.querySelector("#app"));

window.addEventListener("popstate", (ev) => router.route());

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("click", e => {
        if ((<Element>e.target).matches("[data-link]")) {
            e.preventDefault();
            router.navigateTo((<Element>e.target)["href"]);
            (<Element>e.target).parentElement.querySelectorAll('.nav-link.active').forEach(el => el.classList.remove('active'));
            (<Element>e.target).classList.add('active');
        }
    });    
    router.route(); 
    let links = document.querySelectorAll('.nav .nav-link[data-link]');   
    for(let link in links){
        let el = links[link];
        let url = el.attributes["href"].value;
        let res = router.getMatch(url);            
        if(res.route.path == router.currentRoute){
            (<Element>el).classList.add('active');
            break;
        }
    }
    
});