export interface MetaHeader {
    name: string;
    version: number;
    records: number;
    timestamp: number;
}