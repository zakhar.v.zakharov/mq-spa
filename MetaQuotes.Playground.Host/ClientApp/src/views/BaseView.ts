import BaseComponent from "../components/BaseComponent";
import Router from "../core/Router";

export default abstract class extends BaseComponent {
    constructor(protected appContainer: Element, protected router: Router, protected params: any) {
        super(appContainer);
    }

    public spinnerState(state) {
        if (state) {
            document.body.classList.remove('loaded');
        }
        else {
            document.body.classList.add('loaded');
        }
    }

    public async render() {
        var markup = this.markup();
        markup += '<div class="spinner"></div>';
        this.container.innerHTML = markup;
    }

}