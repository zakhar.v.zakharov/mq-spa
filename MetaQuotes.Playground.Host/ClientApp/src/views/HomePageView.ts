import Services from "../data/Services";
import Router from "../core/Router";
import BaseView from "./BaseView";
import {MetaHeader} from '../models/MetaHeader';

export default class extends BaseView {
    constructor(protected appContainer:Element, 
                protected router:Router, 
                protected params:any) {
        super(appContainer, router, params);        
        this.setTitle("Home");
    }
    
    header:MetaHeader;
    async render() {
        this.header = await Services.getDbMetaInfo();
        await super.render();
    }

    formatDate(timeStamp:number) {
        var options = { year: 'numeric', 
                        month: 'long', 
                        day: 'numeric', 
                        hour: 'numeric', 
                        minute: 'numeric', 
                        second: 'numeric' 
                    };
        var date = new Date(timeStamp * 1000);
        var f = new Intl.DateTimeFormat('en', <any>options);
        return f.format(date);
    }

    markup() {
        return `
            <h1>Metaquotes Test Task</h1>
            <p>
                Database meta information
            </p>
            <table>
                <tr>
                    <td class="header">Database Name</td>
                    <td>${this.header.name}</td>
                </tr>
                <tr>
                    <td class="header">Database Version</td>
                    <td>${this.header.version}</td>
                </tr>
                <tr>
                    <td class="header">Timestamp</td>
                    <td>${this.formatDate(this.header.timestamp)}</td>
                </tr>
                <tr>
                    <td class="header">Number of records</td>
                    <td>${this.header.records}</td>
                </tr>
            </table>
        `;
    }
}