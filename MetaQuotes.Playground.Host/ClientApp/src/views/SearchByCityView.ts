import Services from "../data/Services";
import SearchBoxComponent, { SearchBoxComponentOptions } from "../components/shared/SearchBoxComponent";
import GridComponent, { GridComponentOptions } from "../components/shared/GridComponent";
import Router from "../core/Router";
import BaseView from "./BaseView";

export default class extends BaseView {
    constructor(protected appContainer: Element,
        protected router: Router,
        protected params: any) {
        super(appContainer, router, params);
        this.setTitle("Search Locations By City");
        if (params.city)
            this.city = unescape(params.city);
    }

    markup() {
        return `                        
            <p>This page is for searching locations by city</p>
            <div class="search-div"></div>
            <div class="content-block"></div>
        `;
    }

    private city: string;
    private grid: GridComponent;
    private data: any[];

    private async loadData() {
        if (this.city) {
            this.data = await Services.getLocationsByCityName(this.city);
        }
        else {
            this.data = null;
        }
        this.grid.renderData(this.data);
    }

    private async renderSearchBox() {
        let div = this.container.querySelector('.search-div');
        let searchBox = new SearchBoxComponent(div, <SearchBoxComponentOptions>{
            initValue: this.city,
            placeholder: 'Type city name...'
        });
        await searchBox.render();
        searchBox.subscribe(async text => { await this.searchLocations(text); });
    }

    private initGrid() {
        let contentBlock = this.container.querySelector('.content-block');
        this.grid = new GridComponent(contentBlock, <GridComponentOptions>{
            columns: [
                { header: 'Country', key: 'country' },
                { header: 'Region', key: 'region' },
                { header: 'City', key: 'city' },
                { header: 'Postal', key: 'country' },
                { header: 'Organization', key: 'organization' },
                { header: 'Latitude', key: 'latitude' },
                { header: 'Longitude', key: 'longitude' }
            ]
        });
    }

    async render() {
        await super.render();
        await this.renderSearchBox();
        this.initGrid();
        await this.loadData();

    }

    async searchLocations(text) {
        if (text != this.city)
            this.router.setRoute(this.router.currentRoute, { city: text });
        this.city = text;
        await this.loadData();
    }
}