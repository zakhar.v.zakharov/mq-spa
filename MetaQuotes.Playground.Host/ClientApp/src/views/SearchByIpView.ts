import Services from "../data/Services";
import SearchBoxComponent, { SearchBoxComponentOptions } from "../components/shared/SearchBoxComponent";
import GridComponent, { GridComponentOptions } from "../components/shared/GridComponent";
import Router from "../core/Router";
import BaseView from "./BaseView";

export default class extends BaseView {
    constructor(protected appContainer: Element,
        protected router: Router,
        protected params: any) {
        super(appContainer, router, params);
        this.setTitle("Search Locations By Ip");
        this.ip = params.ip;
    }

    markup() {
        return `                        
            <p>This page is for searching locations by Ip</p>
            <div class="search-div"></div>
            <div class="content-block"></div>
        `;
    }
    private grid: GridComponent;
    private ip: string;
    private data: any[];

    async loadData() {
        if (this.ip) {
            this.data = await Services.getLocationsByIp(this.ip);
        }
        else {
            this.data = null;
        }
        this.grid.renderData(this.data);
    }



    private async renderSearchBox() {
        var div = this.container.querySelector('.search-div');
        var searchBox = new SearchBoxComponent(div, <SearchBoxComponentOptions>{
            initValue: this.ip,
            placeholder: 'Type ip address...',
            pattern: '^((\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])$'
        });
        searchBox.subscribe(async text => { await this.searchLocations(text); });
        await searchBox.render();
    }

    private initGrid() {
        var contentBlock = this.container.querySelector('.content-block');
        this.grid = new GridComponent(contentBlock, <GridComponentOptions>{
            columns: [
                { header: 'Country', key: 'country' },
                { header: 'Region', key: 'region' },
                { header: 'City', key: 'city' },
                { header: 'Postal', key: 'country' },
                { header: 'Organization', key: 'organization' },
                { header: 'Latitude', key: 'latitude' },
                { header: 'Longitude', key: 'longitude' }
            ]
        });
    }

    async render() {
        await super.render();
        await this.renderSearchBox();
        this.initGrid();
        await this.loadData();
    }

    private async searchLocations(text) {
        if (text != this.ip)
            this.router.setRoute(this.router.currentRoute, { ip: text });
        this.ip = text;
        await this.loadData();
    }
}