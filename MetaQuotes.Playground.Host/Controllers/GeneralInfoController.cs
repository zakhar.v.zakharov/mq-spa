﻿using MetaQuotes.Playground.Database.Models.Unsafe;
using MetaQuotes.Playground.Services.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetaQuotes.Playground.Host.Controllers
{
    [ApiController]
    public class GeneralInfoController: ControllerBase
    {
        private readonly ILogger<GeneralInfoController> _logger;
        private readonly IGeneralInfoService _generalInfoService;

        public GeneralInfoController(IGeneralInfoService generalInfoService, ILogger<GeneralInfoController> logger)
        {
            _generalInfoService = generalInfoService;
            _logger = logger;
        }

        [HttpGet("meta/db")]
        public Header GetHeader()
        {
            var result = _generalInfoService.GetDatabaseMetaInfo();
            return result;
        }

    }
}
