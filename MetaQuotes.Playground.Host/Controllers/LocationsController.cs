﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MetaQuotes.Playground.Database.Models.Unsafe;
using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Host.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MetaQuotes.Playground.Host.Controllers
{
    [ApiController]    
    public class LocationsController : ControllerBase
    {
        
        private readonly ILogger<LocationsController> _logger;
        private readonly ILocationService _locationSearcher;

        public LocationsController(ILocationService locationSearcher, ILogger<LocationsController> logger)
        {
            _logger = logger;
            _locationSearcher = locationSearcher;
        }

        [HttpGet("city/locations")]
        public IActionResult LocationsByCityName(string city)
        {
            return Ok(_locationSearcher.GetLocationsByCityName(city));            
        }

      
        [HttpGet("/ip/location")]
        public IActionResult LocationsByIp(string ip)
        {
            if (!IpUtils.TryParseToUInt(ip, out uint ipAddress))
            {
                return BadRequest("Ip Address format is not valid");
            }
            return Ok(_locationSearcher.GetLocationsByIpRange(ipAddress));
        }
    }
}
