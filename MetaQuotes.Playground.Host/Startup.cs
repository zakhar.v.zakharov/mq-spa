using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MetaQuotes.Playground.Services;

namespace MetaQuotes.Playground.Host
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private readonly string _clientAssets = "ClientApp\\dist";


        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddControllers();            
            services.AddLogging()
                    .AddOptions()
                    .RegisterServices(Configuration.GetConnectionString("LocationsDbFileName"));

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = _clientAssets;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();                
            }
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();                
            });

            app.UseSpa(spa => {
                spa.Options.SourcePath = _clientAssets;
                spa.Options.DefaultPageStaticFileOptions = new StaticFileOptions
                {
                    OnPrepareResponse = context =>
                    {
                        // never cache index.html
                        if (context.File.Name == "index.html")
                        {
                            context.Context.Response.Headers.Add("Cache-Control", "no-cache, no-store");
                            context.Context.Response.Headers.Add("Expires", "-1");
                        }
                    }
                };
            });


        }
    }
}
