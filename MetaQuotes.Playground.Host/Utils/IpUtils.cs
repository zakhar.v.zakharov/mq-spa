﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MetaQuotes.Playground.Host.Utils
{
    public class IpUtils
    {
        public static bool TryParseToUInt(string ipAddress, out uint result)
        {
            IPAddress address;
            if (!IPAddress.TryParse(ipAddress, out address))
            {
                result = 0;
                return false;
            }            
            byte[] bytes = address.GetAddressBytes();

            // flip big-endian(network order) to little-endian
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
            result =  BitConverter.ToUInt32(bytes, 0);
            return true;
        }
    }
}
