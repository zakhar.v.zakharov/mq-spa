﻿using MetaQuotes.Playground.Database.Models.Unsafe;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Abstractions
{
    public interface IGeneralInfoService
    {
        Header GetDatabaseMetaInfo();
    }
}
