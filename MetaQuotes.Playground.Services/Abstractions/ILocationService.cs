﻿using MetaQuotes.Playground.Database.Models.Unsafe;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Abstractions
{
    public interface ILocationService
    {
        /// <summary>
        /// Retrieve locations by city name (with respect to string case)
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <returns>Collection of Locations</returns>
        IEnumerable<Location> GetLocationsByCityName(string cityName);

        /// <summary>
        /// Retrieve locations by ip address. It is assumed that multiple cities may have the same ip address range
        /// </summary>
        /// <param name="ip">Ip address in uint</param>
        /// <returns></returns>
        IEnumerable<Location> GetLocationsByIpRange(uint ip);
    }
}
