﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Abstractions
{
    public interface ISearchFactory
    {
        /// <summary>
        /// Retrieve search service by type
        /// </summary>
        /// <typeparam name="T">Type of search service</typeparam>
        /// <returns>Search service</returns>
        ISearchService<T> GetSearchService<T>();
    }
}
