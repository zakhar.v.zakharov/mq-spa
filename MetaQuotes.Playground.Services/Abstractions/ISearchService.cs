﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Abstractions
{
    public interface ISearchService<U>
    {
        /// <summary>
        /// Searches value(s) in collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection to search</param>
        /// <param name="value">value to search</param>
        /// <param name="getValue">functions to extract value from collection element</param>
        /// <param name="indexes">Index of sorted collection. Pass null if collection is already ordered</param>
        /// <returns>Collection of all occurances</returns>
        IEnumerable<T> BinarySearchByValue<T>(T[] collection, U value, Func<T, U> getValue, uint[] indexes = null);

        /// <summary>
        /// Searches value in collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection to search</param>
        /// <param name="value">value to search</param>
        /// <param name="getValue">functions to extract value from collection element</param>
        /// <param name="indexes">Index of sorted collection. Pass null if collection is already ordered</param>
        /// <returns>Index of closest occurance</returns>
        int BinarySearchGetClosestIndex<T>(T[] collection, U value, Func<T, U> getValue, uint[] indexes = null);
    }
}
