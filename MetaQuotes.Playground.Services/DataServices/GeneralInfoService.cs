﻿using MetaQuotes.Playground.Database;
using MetaQuotes.Playground.Database.Models.Unsafe;
using MetaQuotes.Playground.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services
{
    public class GeneralInfoService : IGeneralInfoService
    {
        private readonly IDataContext _dataContext;
        public GeneralInfoService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Header GetDatabaseMetaInfo()
        {
            return _dataContext.Header;
        }
    }
}
