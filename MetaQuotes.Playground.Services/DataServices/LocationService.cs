﻿using MetaQuotes.Playground.Database;
using MetaQuotes.Playground.Services.Extensions;
using MetaQuotes.Playground.Database.Models.Unsafe;
using MetaQuotes.Playground.Services.Abstractions;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System;

namespace MetaQuotes.Playground.Services
{
    public class LocationService : ILocationService
    {
        private readonly IDataContext _dataContext;     
        private readonly ILogger<LocationService> _logger;
        private readonly ISearchFactory _searchFactory;
        public LocationService(IDataContext dataContext, ISearchFactory searchFactory, ILogger<LocationService> logger)
        {
            _dataContext = dataContext;            
            _logger = logger;
            _searchFactory = searchFactory;
        }
        public IEnumerable<Location> GetLocationsByCityName(string cityName)
        {
            var service = _searchFactory.GetSearchService<string>();
            var locations = service.BinarySearchByValue(_dataContext.Locations, cityName, l => l.City, _dataContext.ByCityLocationIndex);            
            return locations;
        }

        public IEnumerable<Location> GetLocationsByIpRange(uint ip)
        {
            var service = _searchFactory.GetSearchService<ValueTuple<uint,uint>>();            
            var ranges = service.BinarySearchByValue(_dataContext.Ranges, (ip, ip), r => (r.IpFrom, r.IpTo));
            var locations = ranges.Select(r => _dataContext.Locations[r.LocationIndex]);
            return locations;
        }
    }
}
