﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Extensions
{
    public static class IndexExtensions
    {  
        /// <summary>
        /// Gets item based on index
        /// </summary>
        /// <typeparam name="T">Type of item</typeparam>
        /// <param name="collection">Target collection, containing elements</param>
        /// <param name="index">index of element</param>
        /// <param name="indexes">Collection of indexes. optional</param>
        /// <returns></returns>
        public static T GetItem<T>(this T[] collection, int index, uint[] indexes = null)
        {
            var val = default(T);
            if (indexes != null)
            {
                if (indexes.Length > index && index >= 0)
                    val = collection[indexes[index]];
            }
            else
            {
                if (collection.Length > index && index >= 0)
                    val = collection[index];
            }
            return val;
        }
    }
}
