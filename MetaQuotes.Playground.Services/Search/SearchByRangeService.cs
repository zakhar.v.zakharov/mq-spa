﻿using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Search
{
    public class SearchByRangeService : ISearchService<ValueTuple<uint, uint>>
    {
        /// <summary>
        /// Searches value(s) in collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection to search</param>
        /// <param name="value">value tuple to search. Current implementation doesn't support to search range inside ranges. Only point search is supported.</param>
        /// <param name="getValue">functions to extract value from collection element</param>
        /// <param name="indexes">Index of sorted collection. Pass null if collection is already ordered</param>
        /// <returns>Collection of all occurances</returns>
        public virtual IEnumerable<T> BinarySearchByValue<T>(T[] collection, ValueTuple<uint, uint> value, Func<T, ValueTuple<uint, uint>> getValue, uint[] indexes = null)
        {
            if (value.Item1 != value.Item2)
                throw new NotImplementedException("Searching range inside ranges is not implemented");
            var val = value.Item1;
            List<T> result = new List<T>();
            int closestIndex = BinarySearchGetClosestIndex(collection, value, getValue);
            if (closestIndex != -1)
            {
                var item = collection.GetItem(closestIndex, indexes);
                result.Add(item);
                int index = closestIndex - 1;                
                while (index > 0 && (item = collection.GetItem(index, indexes)) != null && val >= getValue(item).Item1 && val <= getValue(item).Item2)
                {
                    result.Add(item);
                    index--;                    
                }
                index = closestIndex + 1;                
                while (index < collection.Length && (item = collection.GetItem(index, indexes)) != null && val >= getValue(item).Item1 && val <= getValue(item).Item2)
                {
                    result.Add(item);
                    index++;                    
                }
            }
            return result;
        }

        /// <summary>
        /// Searches value(s) in collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection to search</param>
        /// <param name="value">value tuple to search. Current implementation doesn't support to search range inside ranges. Only point search is supported.</param>
        /// <param name="getValue">functions to extract value from collection element</param>
        /// <param name="indexes">Index of sorted collection. Pass null if collection is already ordered</param>
        /// <returns>Index of closest occurance</returns>
        public virtual int BinarySearchGetClosestIndex<T>(T[] collection, ValueTuple<uint, uint> value, Func<T, ValueTuple<uint, uint>> getValue, uint[] indexes = null)
        {
            if (value.Item1 != value.Item2)
                throw new NotImplementedException("Searching range inside ranges is not implemented");

            var val = value.Item1;

            int l = 0, r = collection.Length - 1;
            while (l <= r)
            {
                int m = l + (r - l) / 2;

                var item = collection.GetItem(m, indexes);

                long resStart = (long)val - (long)getValue(item).Item1;
                long resEnd = (long)getValue(item).Item2 - (long)val;

                if (resStart >= 0 && resEnd >= 0)
                    return m;
                if (resEnd < 0)
                    l = m + 1;
                else if (resStart < 0)
                    r = m - 1;
            }

            return -1;
        }
    }
}
