﻿using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.Services.Search
{
    public class SearchByStringService : ISearchService<string>
    {
        /// <summary>
        /// Searches value(s) in collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection to search in</param>
        /// <param name="value">string value to search</param>
        /// <param name="getValue">functions to extract value from collection element</param>
        /// <param name="indexes">Index of sorted collection. Pass null if collection is already ordered</param>
        /// <returns>Collection of all occurances</returns>
        public virtual IEnumerable<T> BinarySearchByValue<T>(T[] collection, string value, Func<T, string> getValue, uint[] indexes = null)
        {
            var closestIndex = BinarySearchGetClosestIndex(collection, value, getValue, indexes);

            List<T> result = new List<T>();
            if (closestIndex != -1)
            {
                var item = collection.GetItem(closestIndex, indexes);
                result.Add(item);                
                int index = closestIndex - 1;               
                while (index > 0 && (item = collection.GetItem(index, indexes)) != null && value == getValue(item))
                {
                    result.Add(item);
                    index--;                    
                }
                index = closestIndex + 1;                
                while (index < indexes.Length && (item = collection.GetItem(index, indexes)) != null && value == getValue(item))
                {
                    result.Add(item);
                    index++;                    
                }
            }
            return result;
        }

        /// <summary>
        /// Searches value(s) in collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection to search in</param>
        /// <param name="value">string value to search</param>
        /// <param name="getValue">functions to extract value from collection element</param>
        /// <param name="indexes">Index of sorted collection. Pass null if collection is already ordered</param>
        /// <returns>Index of closest occurance</returns>
        public virtual int BinarySearchGetClosestIndex<T>(T[] collection, string value, Func<T, string> getValue, uint[] indexes = null)
        {
            int l = 0, r = indexes.Length - 1;            
            while (l <= r)
            {
                int m = l + (r - l) / 2;

                T item = collection.GetItem<T>(m, indexes);

                int res = value.CompareTo(getValue(item));

                if (res == 0)
                    return m;

                if (res > 0)
                    l = m + 1;
                else
                    r = m - 1;
            }

            return -1;
        }
    }
}
