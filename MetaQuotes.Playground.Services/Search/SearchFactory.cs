﻿using MetaQuotes.Playground.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace MetaQuotes.Playground.Services.Search
{
    public class SearchFactory : ISearchFactory
    {

        IServiceProvider _provider;
        public SearchFactory(IServiceProvider provider)
        {
            _provider = provider;
        }

        public ISearchService<T> GetSearchService<T>()
        {
            return _provider.GetService<ISearchService<T>>();
        }
    }
}
