﻿using MetaQuotes.Playground.Database;
using MetaQuotes.Playground.Database.Models;
using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Services.Search;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace MetaQuotes.Playground.Services
{
    public static class Startup
    {
        /// <summary>
        /// Register services
        /// </summary>
        /// <param name="services">IServiceCollection object</param>
        /// <param name="fileName">Path to database file</param>
        /// <returns></returns>
        public static IServiceCollection RegisterServices(this IServiceCollection services, string fileName)        
        {            
            services 
                    .AddSingleton(new DatabaseOptions(fileName, true))                    
                    .AddSingleton<IDataContext, DataContext>()             
                    .AddLogging()                    
                    .AddScoped<ISearchService<string>, SearchByStringService>()
                    .AddScoped<ISearchService<ValueTuple<uint,uint>>, SearchByRangeService>()
                    .AddScoped<ISearchFactory, SearchFactory>()
                    .AddScoped<ILocationService, LocationService>()
                    .AddScoped<IGeneralInfoService, GeneralInfoService>();            
            return services;
        }
    }
}
