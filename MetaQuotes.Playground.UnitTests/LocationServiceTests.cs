﻿using MetaQuotes.Playground.Database;
using MetaQuotes.Playground.Database.Models.Unsafe;
using MetaQuotes.Playground.Services;
using MetaQuotes.Playground.Services.Abstractions;
using MetaQuotes.Playground.Services.Search;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MetaQuotes.Playground.UnitTests
{
    public class LocationServiceTests
    {
        [Fact]
        public void GetLocationsByCityName_GetLocationsByCityName_Calls_SearchService()
        {
            //Arrange
            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(d => d.Locations).Returns(new List<Location>().ToArray());
            dataContextMock.Setup(d => d.ByCityLocationIndex).Returns(new List<uint>().ToArray());
            var loggerMock = new Mock<ILogger<LocationService>>();
            var searchFactoryMock = new Mock<ISearchFactory>();
            var searchStringServiceMock = new Mock<ISearchService<string>>();
            searchStringServiceMock.Setup(s => s.BinarySearchByValue(It.IsAny<Location[]>(), It.IsAny<string>(), It.IsAny<Func<Location, string>>(), It.IsAny<uint[]>())).Returns(new List<Location>());
            searchFactoryMock.Setup(d => d.GetSearchService<string>()).Returns(searchStringServiceMock.Object);
            LocationService service = new LocationService(dataContextMock.Object, searchFactoryMock.Object, loggerMock.Object);
            //Act
            var result = service.GetLocationsByCityName("some string");
            //Assert
            dataContextMock.Verify(d => d.Locations, Times.AtLeastOnce);
            searchFactoryMock.Verify(d => d.GetSearchService<string>(), Times.AtLeastOnce);
            searchStringServiceMock.Verify(s => s.BinarySearchByValue(It.IsAny<Location[]>(), It.IsAny<string>(), It.IsAny<Func<Location, string>>(), It.IsAny<uint[]>()), Times.Once);
        }

        [Fact]
        public void GetLocationsByCityName_GetLocationsByIpRange_Calls_SearchService()
        {
            //Arrange
            var dataContextMock = new Mock<IDataContext>();
            dataContextMock.Setup(d => d.Locations).Returns(new List<Location>().ToArray());
            dataContextMock.Setup(d => d.Ranges).Returns(new List<IpRange>().ToArray());
            var loggerMock = new Mock<ILogger<LocationService>>();
            var searchFactoryMock = new Mock<ISearchFactory>();
            var searchRangeServiceMock = new Mock<ISearchService<(uint,uint)>>();
            searchRangeServiceMock.Setup(s => s.BinarySearchByValue(It.IsAny<IpRange[]>(), It.IsAny<(uint, uint)>(), It.IsAny<Func<IpRange, (uint, uint)>>(), It.IsAny<uint[]>())).Returns(new List<IpRange>());
            searchFactoryMock.Setup(d => d.GetSearchService<(uint, uint)>()).Returns(searchRangeServiceMock.Object);
            LocationService service = new LocationService(dataContextMock.Object, searchFactoryMock.Object, loggerMock.Object);
            //Act
            var result = service.GetLocationsByIpRange(1234);
            //Assert
            dataContextMock.Verify(d => d.Ranges, Times.AtLeastOnce);
            searchFactoryMock.Verify(d => d.GetSearchService<(uint, uint)>(), Times.AtLeastOnce);
            searchRangeServiceMock.Verify(s => s.BinarySearchByValue(It.IsAny<IpRange[]>(), It.IsAny<(uint, uint)>(), It.IsAny<Func<IpRange, (uint, uint)>>(), It.IsAny<uint[]>()), Times.Once);
        }
    }
}
