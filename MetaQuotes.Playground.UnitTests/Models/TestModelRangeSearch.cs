﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MetaQuotes.Playground.UnitTests.Models
{
    public class TestModelRangeSearch
    {
        public uint Id { get; set; }
        public uint From { get; set; }
        public uint To { get; set; }
    }
}
