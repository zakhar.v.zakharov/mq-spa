﻿using MetaQuotes.Playground.Services.Search;
using MetaQuotes.Playground.UnitTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MetaQuotes.Playground.UnitTests
{
    public class SearchByRangeServiceTests
    {

        private SearchByRangeService Service
        {
            get => new SearchByRangeService();
        }

        private List<TestModelRangeSearch> GetTestDataRanges(string csv)
        {
            var list = new List<TestModelRangeSearch>();
            var lines = csv.Split("\n", StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in lines)
            {
                var rngArray = line.Split(';');
                var range = new TestModelRangeSearch
                {
                    Id = uint.Parse(rngArray[0]),
                    From = uint.Parse(rngArray[1]),
                    To = uint.Parse(rngArray[2])
                };
                list.Add(range);
            }
            return list;
        }

        [Fact]
        public void BinarySearchGetClosestIndex_BaseCase_ShouldReturn_Index()
        {
            var csv = "0;8;55472\n1; 55473; 151737\n2; 151738; 191323\n3; 191324; 266940\n4; 266941; 315571\n5; 315572; 388324\n6; 388325; 388325\n7; 388326; 388326\n8; 388327; 388327\n9; 388328; 457815\n10; 457823; 502673";
            var list = GetTestDataRanges(csv).ToArray();
            var index = Service.BinarySearchGetClosestIndex(list, (266942, 266942), c => (c.From, c.To));                
            Assert.Equal(4, index);
            Assert.Equal((uint)4, list[index].Id);
            index = Service.BinarySearchGetClosestIndex(list, (151740, 151740), c => (c.From, c.To));                
            Assert.Equal(2, index);
            Assert.Equal((uint)2, list[index].Id);
        }

        [Fact]
        public void BinarySearchGetClosestIndex_Boundaries_ShouldReturn_Index()
        {
            var csv = "0;8;55472\n1; 55473; 151737\n2; 151738; 191323\n3; 191324; 266940\n4; 266941; 315571\n5; 315572; 388324\n6; 388325; 388325\n7; 388326; 388326\n8; 388327; 388327\n9; 388328; 457815\n10; 457823; 502673";
            var list = GetTestDataRanges(csv).ToArray();
            var index = Service.BinarySearchGetClosestIndex(list, (266941, 266941), c => (c.From, c.To));                
            Assert.Equal(4, index);
            Assert.Equal((uint)4, list[index].Id);
            index = Service.BinarySearchGetClosestIndex(list, (388324, 388324), c => (c.From, c.To));            
            Assert.Equal(5, index);
            Assert.Equal((uint)5, list[index].Id);
        }

        [Fact]
        public void BinarySearchGetClosestIndex_OneBigInterval_ShouldReturn_Index()
        {
            var csv = $"0;0;{uint.MaxValue}";
            var list = GetTestDataRanges(csv).ToArray();
            var index = Service.BinarySearchGetClosestIndex(list, (266941, 266941), c => (c.From, c.To));
            //list.BinarySearchByRangeSorted(266941, c => c.From, c => c.To);
            Assert.Equal(0, index);
        }


        [Fact]
        public void BinarySearchGetClosestIndex_OutOfRanges_ShouldReturn_MinusOne()
        {
            var csv = "0;8;55472\n1; 55473; 151737\n2; 151738; 191323\n3; 191324; 266940\n4; 266941; 315571\n5; 315572; 388324\n6; 388325; 388325\n7; 388326; 388326\n8; 388327; 388327\n9; 388328; 457815\n10; 457823; 502673";
            var list = GetTestDataRanges(csv).ToArray();
            var index = Service.BinarySearchGetClosestIndex(list, (502674, 502674), c => (c.From, c.To));            
            Assert.Equal(-1, index);
        }

        [Fact]
        public void BinarySearchByValue_BaseCase_ShouldReturn_Ranges()
        {
            var csv = "0;8;55472\n1; 55473; 151737\n2; 151738; 191323\n3; 191310; 266940\n4; 266941; 315571\n5; 315572; 388324\n6; 388325; 388325\n7; 388326; 388326\n8; 388327; 388327\n9; 388328; 457815\n10; 457823; 502673";
            var list = GetTestDataRanges(csv).ToArray();
            var ranges = Service.BinarySearchByValue(list, (191315, 191315), c => (c.From, c.To)).ToList();                
            Assert.Equal(2, ranges.Count);
            Assert.Contains(ranges, el => el.Id == 2);
            Assert.Contains(ranges, el => el.Id == 3);
        }
       
    }
}
