﻿using MetaQuotes.Playground.Services.Search;
using MetaQuotes.Playground.UnitTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MetaQuotes.Playground.UnitTests
{
    public class SearchByStringServiceTests
    {
        private List<TestModelStringSearch> GetDataForIndex(string csv)
        {
            var list = csv.Split(';').Select(c => new TestModelStringSearch { Name = c }).ToList();
            return list;
        }

        private SearchByStringService Service
        {
            get => new SearchByStringService();
        }

        [Fact]
        public void BinarySearchGetClosestIndex_EvenLength_ShouldReturn_Index()
        {                        
            var list = GetDataForIndex("Cn;An;Nnnsda;Bn12;Jasda;E443r;Vkkasd;Vakkasd");            
            Assert.Equal(0, list.Count % 2);
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var index = Service.BinarySearchGetClosestIndex(list.ToArray(), "Jasda", m => m.Name, sorted.Select(c => c.Index).ToArray());            
            Assert.Equal(4, index);
            index = Service.BinarySearchGetClosestIndex(list.ToArray(), "Vkkasd", m => m.Name, sorted.Select(c => c.Index).ToArray());            
            Assert.Equal(7, index);
        }

        [Fact]
        public void BinarySearchGetClosestIndex_OddLength_ShouldReturn_Index()
        {            
            var list = GetDataForIndex("Cn;An;Nnnsda;Bn12;Jasda;E443r;Vkkasd;Vakkasd;Mlkin");
            Assert.Equal(1, list.Count % 2);
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var index = Service.BinarySearchGetClosestIndex(list.ToArray(), "Jasda", m => m.Name, sorted.Select(c => c.Index).ToArray());            
            Assert.Equal(4, index);
            index = Service.BinarySearchGetClosestIndex(list.ToArray(), "Vkkasd", m => m.Name, sorted.Select(c => c.Index).ToArray());            
            Assert.Equal(8, index);
        }

        [Fact]
        public void BinarySearchGetClosestIndex_AtTheEnds_ShouldReturn_Index()
        {
            var list = GetDataForIndex($"Cn;An;Nnnsda;Bn12;Jasda;E443r;Vkkasd;Vakkasd;Mlkin");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var index = Service.BinarySearchGetClosestIndex(list.ToArray(), sorted.First().Name, m => m.Name, sorted.Select(c => c.Index).ToArray());
            Assert.Equal(0, index);
            index = Service.BinarySearchGetClosestIndex(list.ToArray(), sorted.Last().Name, m => m.Name, sorted.Select(c => c.Index).ToArray());
            Assert.Equal(list.Count - 1, index);
        }

        [Fact]
        public void BinarySearchGetClosestIndex_DifferentCase_ShouldReturn_MinusOne()
        {            
            var list = GetDataForIndex("Cn;An;Nnnsda;Bn12;Jasda;E443r;Vkkasd;Vakkasd");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var index = Service.BinarySearchGetClosestIndex(list.ToArray(), "E443R", m => m.Name, sorted.Select(c => c.Index).ToArray());                
            Assert.Equal(-1, index);
        }

        [Fact]
        public void BinarySearchGetClosestIndex_HasEmptyString_ShouldReturn_Index()
        {
            var list = GetDataForIndex("Cn;An;Nnnsda;Bn12;Jasda;E443r;;Vakkasd");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var index = Service.BinarySearchGetClosestIndex(list.ToArray(), "", m => m.Name, sorted.Select(c => c.Index).ToArray());
            Assert.Equal(0, index);
            index = Service.BinarySearchGetClosestIndex(list.ToArray(), "Jasda", m => m.Name, sorted.Select(c => c.Index).ToArray());
            Assert.Equal(5, index);
        }

        [Fact]
        public void BinarySearchByValue_ShouldReturn_Occurances()
        {            
            var list = GetDataForIndex("Cn;An;Nnnsda;Bn12;Jasda;Jasda;Vkkasd;Vakkasd");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var data = Service.BinarySearchByValue(list.ToArray(), "Jasda", m => m.Name, sorted.Select(c => c.Index).ToArray()).ToList();                
            Assert.Equal(2, data.Count);
            Assert.Equal("Jasda", data[0].Name);
            Assert.Equal("Jasda", data[1].Name);
        }

        [Fact]
        public void BinarySearchByValue_AllButOne_ShouldReturn_Occurances()
        {
            var list = GetDataForIndex("Cn;An;Cn;Cn;Cn;Cn;Cn;Cn");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var data = Service.BinarySearchByValue(list.ToArray(), "Cn", m => m.Name, sorted.Select(c => c.Index).ToArray()).ToList();
            Assert.Equal(list.Count - 1, data.Count);
            Assert.Equal("Cn", data[0].Name);            
        }        


        [Fact]
        public void BinarySearchByValue_AllRepeated_ShouldReturn_All()
        {
            var list = GetDataForIndex("Sddsd;Sddsd;Sddsd;Sddsd;Sddsd;Sddsd;Sddsd;Sddsd;Sddsd;Sddsd;Sddsd");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var data = Service.BinarySearchByValue(list.ToArray(), "Sddsd", m => m.Name, sorted.Select(c => c.Index).ToArray()).ToList();               
            Assert.All(data, m => Assert.Equal("Sddsd", m.Name));
        }

        [Fact]
        public void BinarySearchByValue_NoResults_ShouldReturn_Empty()
        {
            var list = GetDataForIndex("Cn;An;Nnnsda;Bn12;Jasda;Jasda;Vkkasd;Vakkasd");
            var indices = list.Select((c, i) => new { c.Name, Index = (uint)i }).ToList();
            var sorted = indices.OrderBy(c => c.Name);
            var data = Service.BinarySearchByValue(list.ToArray(), "Njsas", m => m.Name, sorted.Select(c => c.Index).ToArray()).ToList();                
            Assert.Empty(data);
        }
    }
}
