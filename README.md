# mq-spa

This is test task for MetaQuotes interview.

## Getting started

The solution consists of the following projects:

1. **MetaQuotes.Playground.Host** - ASP.NET core application which hosts WebApi and UI (JS) application
2. **MetaQuotes.Playground.Services** - Main application services
3. **MetaQuotes.Playground.Database** - Infrastructure layer, dealing with loading database into memory
4. **MetaQuotes.Playground.UnitTests** - Application unit tests
5. **MetaQuotes.Playground.Benchmarks** - performance measurement project

Client(UI) application is in MetaQuotes.Playground.Host/ClientApp folder and written in TypeScript without usage of any MVC frameworks and engines

## About the task
There is one change in relation to initial task: search by Ip address. Finding an ip address happens with assumption that in real life multiple cities may have the same ip range. So, the web api method GET /ip/location?ip=123.234.123.234 may return the collection of locations.

## Database

Database project uses unsafe code and pointers to parse database file sequential format.

# Client application

For Client App TypeScript was used. 
To build client application, use the following syntax:
```
cd ClientApp
npm i
npm run build:prod
```
For production use:
```
npm run build:prod
```
For development use
```
npm run build:dev
```
or
```
npm run watch:dev
```
Client Application doesn't require "express" or any other Node servers

# Benchmarks

1. Load database into memory
The following report is from BenchmarkDotNet artifacts
``` ini
BenchmarkDotNet=v0.13.1, OS=Windows 10.0.17134.1967 (1803/April2018Update/Redstone4)
Intel Core i7-3770 CPU 3.40GHz (Ivy Bridge), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.300
  [Host]     : .NET Core 3.1.4 (CoreCLR 4.700.20.20201, CoreFX 4.700.20.22101), X64 RyuJIT
  Job-TRZMSH : .NET Core 3.1.4 (CoreCLR 4.700.20.20201, CoreFX 4.700.20.22101), X64 RyuJIT

Platform=AnyCpu  Runtime=.NET Core 3.1  LaunchCount=10  
MaxIterationCount=100  

```
|       Method |     Mean |    Error |   StdDev |   Median |      Max |      Min |  Op/s |
|------------- |---------:|---------:|---------:|---------:|---------:|---------:|------:|
| LoadDatabase | 13.17 ms | 0.183 ms | 0.907 ms | 12.82 ms | 15.88 ms | 12.06 ms | 75.90 |


Density chart:
![image info](Reports/MetaQuotes.Playground.Benchmarks.DatabaseBenchmark-LoadDatabase-facetDensity.png)

We see here, that in most cases mean value is **12.5 ms** (or 12.5*10^-3 seconds) which is almost 4 times less than required

2. Search by city

``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.17134.1967 (1803/April2018Update/Redstone4)
Intel Core i7-3770 CPU 3.40GHz (Ivy Bridge), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.300
  [Host]     : .NET Core 3.1.4 (CoreCLR 4.700.20.20201, CoreFX 4.700.20.22101), X64 RyuJIT
  Job-YVZXHZ : .NET Core 3.1.4 (CoreCLR 4.700.20.20201, CoreFX 4.700.20.22101), X64 RyuJIT

Platform=AnyCpu  Runtime=.NET Core 3.1  LaunchCount=2  
MaxIterationCount=20  

```
|       Method |      N |          city |     Mean |    Error |   StdDev |      Max |      Min |     Op/s |
|------------- |------- |-------------- |---------:|---------:|---------:|---------:|---------:|---------:|
| **SearchByCity** |  **20000** |   **cit_Efamin ** | **15.79 μs** | **0.620 μs** | **1.085 μs** | **18.50 μs** | **13.51 μs** | **63,319.1** |
| **SearchByCity** |  **20000** | **cit_Ynynecira** | **13.52 μs** | **0.334 μs** | **0.500 μs** | **14.77 μs** | **12.83 μs** | **73,980.9** |
| **SearchByCity** |  **40000** |   **cit_Efamin ** | **15.06 μs** | **0.832 μs** | **1.479 μs** | **18.87 μs** | **13.56 μs** | **66,393.3** |
| **SearchByCity** |  **40000** | **cit_Ynynecira** | **14.28 μs** | **0.333 μs** | **0.557 μs** | **15.78 μs** | **13.66 μs** | **70,051.8** |
| **SearchByCity** |  **60000** |   **cit_Efamin ** | **14.33 μs** | **0.181 μs** | **0.298 μs** | **15.11 μs** | **13.86 μs** | **69,772.2** |
| **SearchByCity** |  **60000** | **cit_Ynynecira** | **15.21 μs** | **0.793 μs** | **1.258 μs** | **19.22 μs** | **14.36 μs** | **65,727.4** |
| **SearchByCity** |  **80000** |   **cit_Efamin ** | **15.03 μs** | **0.366 μs** | **0.611 μs** | **16.77 μs** | **14.24 μs** | **66,523.9** |
| **SearchByCity** |  **80000** | **cit_Ynynecira** | **15.63 μs** | **0.359 μs** | **0.599 μs** | **17.37 μs** | **14.89 μs** | **63,969.7** |
| **SearchByCity** | **100000** |   **cit_Efamin ** | **15.44 μs** | **0.688 μs** | **1.205 μs** | **19.26 μs** | **14.26 μs** | **64,753.0** |
| **SearchByCity** | **100000** | **cit_Ynynecira** | **16.29 μs** | **0.320 μs** | **0.544 μs** | **17.86 μs** | **15.59 μs** | **61,402.4** |

Density chart:
![image info](Reports/MetaQuotes.Playground.Benchmarks.SearchByStringBenchmark-SearchByCity-facetDensity.png)

In most density we have about 15 us per operation (or 15*10^-6 seconds) and about 62,000 operations per second. Test was made against 20000,40000,60000,80000 and 100000 elements in array and the parameter affect is minimal, which confirms logarythmic time.

3. Search by Ip

``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.17134.1967 (1803/April2018Update/Redstone4)
Intel Core i7-3770 CPU 3.40GHz (Ivy Bridge), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.300
  [Host]     : .NET Core 3.1.4 (CoreCLR 4.700.20.20201, CoreFX 4.700.20.22101), X64 RyuJIT
  Job-EEKIXH : .NET Core 3.1.4 (CoreCLR 4.700.20.20201, CoreFX 4.700.20.22101), X64 RyuJIT

Platform=AnyCpu  Runtime=.NET Core 3.1  LaunchCount=2  
MaxIterationCount=20  

```
|     Method |      N |         ip |     Mean |     Error |    StdDev |      Max |      Min |      Op/s |
|----------- |------- |----------- |---------:|----------:|----------:|---------:|---------:|----------:|
| **SearchByIp** |  **20000** | **1422660825** | **3.385 μs** | **0.0797 μs** | **0.1264 μs** | **3.790 μs** | **3.144 μs** | **295,411.4** |
| **SearchByIp** |  **20000** | **3385307463** | **3.680 μs** | **0.1742 μs** | **0.3005 μs** | **4.569 μs** | **3.292 μs** | **271,706.1** |
| **SearchByIp** |  **40000** | **1422660825** | **3.928 μs** | **0.2033 μs** | **0.3507 μs** | **4.899 μs** | **3.355 μs** | **254,597.8** |
| **SearchByIp** |  **40000** | **3385307463** | **3.896 μs** | **0.1226 μs** | **0.1944 μs** | **4.242 μs** | **3.535 μs** | **256,655.3** |
| **SearchByIp** |  **60000** | **1422660825** | **3.645 μs** | **0.0934 μs** | **0.1561 μs** | **4.084 μs** | **3.328 μs** | **274,352.6** |
| **SearchByIp** |  **60000** | **3385307463** | **3.423 μs** | **0.1321 μs** | **0.2134 μs** | **3.803 μs** | **3.142 μs** | **292,133.7** |
| **SearchByIp** |  **80000** | **1422660825** | **3.512 μs** | **0.1824 μs** | **0.3098 μs** | **4.345 μs** | **3.150 μs** | **284,731.3** |
| **SearchByIp** |  **80000** | **3385307463** | **3.521 μs** | **0.0935 μs** | **0.1587 μs** | **3.883 μs** | **3.317 μs** | **284,041.3** |
| **SearchByIp** | **100000** | **1422660825** | **3.457 μs** | **0.1077 μs** | **0.1740 μs** | **3.925 μs** | **3.185 μs** | **289,296.1** |
| **SearchByIp** | **100000** | **3385307463** | **3.460 μs** | **0.1437 μs** | **0.2517 μs** | **4.139 μs** | **3.140 μs** | **288,987.6** |

Density chart:

![image info](Reports/MetaQuotes.Playground.Benchmarks.SearchByIpBenchmark-SearchByIp-facetDensity.png)

In most density we have about 3.5 us per operation (or 3.5*10^-6 seconds) and about 280,000 operations per second. Test was made against 20000,40000,60000,80000 and 100000 elements in array and the parameter affect is minimal, which confirms logarythmic time.
